'''将16位图转换为8位图'''
import numpy as np
from PIL import Image
import cv2 as cv

path = "./label/train1_labels_8bits.png"
src = cv.imread(path)
label_array = np.array(src)
print(label_array[83][0][0])
# 创建全0数组表示building
building = np.zeros(label_array.shape)
# 创建全0数组表示vegetable
vegetable = np.zeros(label_array.shape)
# 创建全0数组表示water
water = np.zeros(label_array.shape)
# 创建全0数组表示road
road = np.zeros(label_array.shape)
# 创建全0数组表示other
other_area = np.zeros(label_array.shape)

for i in range(label_array.shape[0]):
    for j in range(label_array.shape[1]):
        if label_array[i][j][0] == 0:
            other_area[i][j][0] = 255
            other_area[i][j][1] = 255
            other_area[i][j][2] = 255
        elif label_array[i][j][0] == 1:
            vegetable[i][j][0] = 159
            vegetable[i][j][1] = 255
            vegetable[i][j][2] = 84
        elif label_array[i][j][0] == 2:
            building[i][j][0] = 34
            building[i][j][1] = 180
            building[i][j][2] = 238
        elif label_array[i][j][0] == 3:
            water[i][j][0] = 255
            water[i][j][1] = 191
            water[i][j][2] = 0
        elif label_array[i][j][0] == 4:
            road[i][j][0] = 38
            road[i][j][1] = 71
            road[i][j][2] = 139
        pass
    pass
# np.save("building.npy", building)
# np.save("road.npy", road)
# np.save("water.npy", water)
# np.save("vegetable.npy", vegetable)
# np.save("other.npy", other_area)
# print(building)

cv.imwrite("./label/water1.png", water)
cv.imwrite("./label/road1.png", road)
cv.imwrite("./label/building1.png", building)
cv.imwrite("./label/vegetable1.png", vegetable)
cv.imwrite("./label/other1.png", other_area)

# print(src)

print(cv.__version__)
