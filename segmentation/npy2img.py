import numpy as np
import cv2

building = np.load("building.npy")
road = np.load("road.npy")
water = np.load("water.npy")
vegetable = np.load("vegetable.npy")
other = np.load("other.npy")

cv2.imwrite("water.png", water)
cv2.imwrite("road.png", road)
cv2.imwrite("building.png", building)
cv2.imwrite("vegetable.png", vegetable)
cv2.imwrite("other.png", other)
